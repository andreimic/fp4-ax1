{- 0907390 Andrei Mustata
 - Functional Programming 4
 - Assessed Exercise 1
 - An interpreter for an Algol-like imperative language, written in Haskell
 - 13 Oct 2012
-}

module First.Main where

import Data.Map (alter, fromList, toList)
import Data.Maybe
import System.IO
import Control.Monad
import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Expr
import Text.ParserCombinators.Parsec.Language
import qualified Text.ParserCombinators.Parsec.Token as Token

type Var = String
type Decl = Var
type Env = [[(Var, Integer)]]

data IntExp
  = IVar  Var
  | ICon  Integer
  | Neg   IntExp
  | IntOperation  IntOp   IntExp  IntExp
  deriving (Show)

data IntOp
  = Add
  | Substract
  | Multiply
  | Divide
  deriving (Show)

data BoolExp
  = BCon Bool
  | Not BoolExp
  | BOperation BOp BoolExp BoolExp
  | ROperation ROp IntExp IntExp
  deriving (Show)

data BOp
  = And
  | Or
  deriving(Show)

data ROp
  = Greater
  | Less
  | Equals
  deriving(Show)

data Stmt
  = Begin [Decl] [Stmt]
  | Assign Var IntExp
  | Read Var
  | Write IntExp
  | IfThenElse BoolExp Stmt Stmt
  | While BoolExp Stmt
  deriving (Show)

{-
 - Environment related
 - lookup, update for Environment
 -}

ilookup :: Var -> Env -> Maybe Integer
ilookup v e
  = lookup v (head e)

iupdate :: Var -> Integer -> Env -> Env
iupdate var val env
  = [Data.Map.toList(Data.Map.alter f var (Data.Map.fromList(head env)))]
    where
      f x = Just val

{-
 - Evaluators
 -}

--evaluates IntExp's
evalIntExp :: IntExp -> Env -> Integer
evalIntExp (IVar v) e
  = fromJust(ilookup v e)
evalIntExp (ICon c) e
  = c
evalIntExp (Neg inte) e
  = - evalIntExp inte e
evalIntExp (IntOperation Add inte1 inte2) e
  = evalIntExp inte1 e + evalIntExp inte2 e
evalIntExp (IntOperation Substract inte1 inte2) e
  = evalIntExp inte1 e - evalIntExp inte2 e
evalIntExp (IntOperation Multiply inte1 inte2) e
  = evalIntExp inte1 e * evalIntExp inte2 e
evalIntExp (IntOperation Divide inte1 inte2) e
  = evalIntExp inte1 e `div` evalIntExp inte2 e

--interprets Stmt's
interpret :: Stmt -> Env -> Env
interpret (Begin decls stmts) env
  = undefined
interpret (Assign var exp) env
  = iupdate var (fromJust(ilookup var env)) env
interpret (Read var) env
  = undefined
interpret (Write exp) env
  = undefined
interpret (IfThenElse be stmtT stmtF) env
  = undefined
interpret (While be stmt) env
  = undefined

{-
 - Parser related
 -}
