module First.Specs where

import Test.Hspec
import First.Main as Main

main = hspec $ do
  describe "ilookup" $ do
    it "returns Just 1 when given 'a' [[('a', 1), ('b', 2)]]" $
      ilookup "a" [[("a", 1), ("b", 2)]] `shouldBe` Just 1

    it "returns Nothing when given 'c' [[('a', 1), ('b', 2)]]" $
      ilookup "c" [[("a", 1), ("b", 2)]] `shouldBe` Nothing

  describe "iupdate" $ do
    it "returns [[('a', 1), ('b', 2)]] when given 'b' 2 [[('a', 1), ('b', 1)]]" $
      Main.iupdate "b" 2 [[("a", 1), ("b", 1)]] `shouldBe` [[("a", 1), ("b", 2)]]

    it "returns [[('a', 1), ('b', 2)]] when given 'b' 2 [[('a', 1)]]" $
      Main.iupdate "b" 2 [[("a", 1)]] `shouldBe` [[("a", 1), ("b", 2)]]

  describe "evalIntExp" $ do
    describe "IVar" $ do
      it "returns 1 when given 'a' [[('a', 1)]]" $
        Main.evalIntExp (IVar "a") [[("a", 1)]] `shouldBe` 1

    describe "ICon" $ do
      it "returns 1 when given 1 [[('a', 1)]]" $
        Main.evalIntExp (ICon 1) [[("a", 1)]] `shouldBe` 1

    describe "Neg" $ do
      it "(IVar) returns -1 when given (Neg IVar('a')) [[('a', 1)]]" $
        Main.evalIntExp (Neg (IVar "a")) [[("a", 1)]] `shouldBe` -1

      it "(ICon) returns -1 when given (Neg ICon(1)) [[('a', 1)]]" $
        Main.evalIntExp (Neg (ICon 1)) [[("a", 1)]] `shouldBe` -1

    describe "Add" $ do
      it "(ICon ICon) returns 2 when given (IntOperation Add (ICon 1) (ICon 1)) [[('a', 1)]]" $
        Main.evalIntExp (IntOperation Add (ICon 1) (ICon 1)) [[("a", 1)]] `shouldBe` 2

      it "(ICon IVar) returns 2 when given (IntOperation Add (ICon 1) (IVar 'a')) [[('a', 1)]]" $
        Main.evalIntExp (IntOperation Add (ICon 1) (IVar "a")) [[("a", 1)]] `shouldBe` 2

      it "(IVar IVar) returns 2 when given (IntOperation Add (IVar 'a') (IVar 'a')) [[('a', 1)]]" $
        Main.evalIntExp (IntOperation Add (IVar "a") (IVar "a")) [[("a", 1)]] `shouldBe` 2

      it "(IVar IVar) returns 2 when given (IntOperation Add (IVar 'a') (IVar 'a')) [[('a', 1)]]" $
        Main.evalIntExp (IntOperation Add (IVar "a") (IVar "b")) [[("a", 1), ("b", 1)]] `shouldBe` 2

    describe "Substract" $ do
      it "(ICon ICon) returns 0 when given (IntOperation Substract (ICon 1) (ICon 1)) [[('a', 1)]]" $
        Main.evalIntExp (IntOperation Substract (ICon 1) (ICon 1)) [[("a", 1)]] `shouldBe` 0

      it "(IVar ICon) returns 0 when given (IntOperation Substract (IVar 'a') (ICon 1)) [[('a', 1)]]" $
        Main.evalIntExp (IntOperation Substract (IVar "a") (ICon 1)) [[("a", 1)]] `shouldBe` 0

      it "(IVar IVar) returns 2 when given (IntOperation Substract (IVar 'a') (IVar 'b')) [[('a', 1), ('b', 1)]]" $
        Main.evalIntExp (IntOperation Substract (IVar "a") (IVar "b")) [[("a", 1), ("b", 1)]] `shouldBe` 0

    describe "Multiply" $ do
      it "(ICon ICon) returns 2 when given (IntOperation Multiply (ICon 1) (ICon 2)) [[('a', 1)]]" $
        Main.evalIntExp (IntOperation Multiply (ICon 1) (ICon 2)) [[("a", 1)]] `shouldBe` 2

      it "(IVar ICon) returns 2 when given (IntOperation Multiply (IVar 'a') (ICon 2)) [[('a', 1)]]" $
        Main.evalIntExp (IntOperation Multiply (IVar "a") (ICon 2)) [[("a", 1)]] `shouldBe` 2

      it "(IVar IVar) returns 6 when given (IntOperation Multiply (IVar 'a') (IVar 'a')) [[('a', 2)]]" $
        Main.evalIntExp (IntOperation Multiply (IVar "a") (IVar "b")) [[("a", 2), ("b", 3)]] `shouldBe` 6

    describe "Divide" $ do
      it "(ICon ICon) returns 2 when given (IntOperation Divide (ICon 4) (ICon 2)) [[('a', 1)]]" $
        Main.evalIntExp (IntOperation Divide (ICon 4) (ICon 2)) [[("a", 1)]] `shouldBe` 2

      it "(IVar ICon) returns 2 when given (IntOperation Divide (IVar 'a') (ICon 2)) [[('a', 1)]]" $
        Main.evalIntExp (IntOperation Divide (IVar "a") (ICon 2)) [[("a", 4)]] `shouldBe` 2

      it "(IVar IVar) returns 2 when given (IntOperation Divide (IVar 'a') (IVar 'b')) [[('a', 6), ('b', 3)]]" $
        Main.evalIntExp (IntOperation Divide (IVar "a") (IVar "b")) [[("a", 6), ("b", 3)]] `shouldBe` 2

  describe "interpret" $ do
    describe "Assign" $ do
      it "returns [[('a', 2)]] when given (Assign 'a' ICon 2) [[('a', 0)]]" $
        Main.interpret (Assign "a" (ICon 2)) [[("a", 2)]] `shouldBe` [[("a", 2)]]

    describe "Begin" $ do
      it "returns [[('a',0), ('b', 0)]] when given (Begin ['a', 'b'] [Stmt]) [[()]]" $
        Main.interpret (Begin ["a", "b"] [(Assign "a" (ICon 2))]) [[]] `shouldBe` [[("a", 2), ("b", 0)]]